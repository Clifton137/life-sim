import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class represents the predator for the simulation
 */
public class Predator extends Animal
{
    private char [] p_aggression;           /* char array of aggressive traits */
    private char[] p_speed;                 /* char array of speed traits */
    private char[]p_power;                  /* char array of power traits */
    private int aggresion;
    private double GEST_PERIOD;                /* const gestation period */
    public int MAX_CHILDREN;               /* const Max number of children for predators */
    private int p_parent_id;                /* predators parents id */
    private boolean p_can_hunt_parent;      /* true if predator can hunt their parents */
    public int offspring_Energy;            /*Energy level assigned to all affsrping of this predator*/
    private int smelldistance=25;           /*distance that a pred can smell grazers*/
    private int ParentID;                   /*Parrent ID for predators*/
    private ArrayList<Integer> ChildrenIDs; //List containing all active children of the predator
    public ArrayList<Predator> UnbornChildren;
    public boolean inGestation;             //bool used to know if a predator is pregnent at the time
    public ArrayList<Animal> visible_food = new ArrayList<Animal>(7);
    public Animal target;
    public int currentGestationTime;
    public Predator mate;
    private GameMaster gameMaster = GameMaster.getInstance();
    public ArrayList<Predator> matingList = new ArrayList<Predator>();


    /**
     * Constructor
     * @param aggessionGene Contains Aggression Gene
     * @param speed         Max run speed
     * @param powerGene     Power gene
     * @param startXPOS     starting x pos
     * @param startYPOS     starting y pos
     * @param energy        contains current energy
     * @param energyoutput  how much energy the pred used
     * @param energytoReproduce energy to reproduce
     * @param maintainSpeedTime How long the pred can maintain max speed
     * @param maxoffspring      max number of offspring
     * @param offspringenergy   offspring starting energy
     * @param gestationtime     amount of time before offspring is born
     */
    public Predator(char[] aggessionGene, double speed, char[] powerGene,
                    double startXPOS, double startYPOS, double energy, double energyoutput ,
                    int energytoReproduce, double maintainSpeedTime,
                    int maxoffspring, int offspringenergy, double gestationtime )
    {
        this.p_aggression = Arrays.copyOf(aggessionGene,2);
        //this.p_speed = Arrays.copyOf(speedGene,2);
        MAX_SPEED=speed;
        this.p_power = Arrays.copyOf(powerGene,2);
        this.X_pos = startXPOS;
        this.Y_pos = startYPOS;
        energy_level=energy;
        movement_EU=energyoutput;
        ENERGY_TO_REPRODUCE=energytoReproduce;
        MAINTAIN_SPEED_TIME=maintainSpeedTime;
        MAX_CHILDREN=maxoffspring;
        offspring_Energy=offspringenergy;
        GEST_PERIOD=gestationtime;
        currentGestationTime=0;
        ChildrenIDs=new ArrayList<Integer>();
        UnbornChildren=new ArrayList<Predator>();
    }

    @Override
    void Reproduce()
    {
        AnimalFactory factory = new AnimalFactory();
        if(mate!=null)
        {
          //  if((Math.abs(mate.getX_pos()-this.getX_pos())<10)&&Math.abs(mate.getY_pos()-this.getY_pos())<10)
            {
                if(mate.mate==this)
                {
                    factory.PredatorReproduce(this,mate);
                    inGestation=true;
                    behavior="Default";
                    mate=null;
                }
            }
        }
    }

    /**
     *
     * @param targetID the ID of the predator that is being hunted
     * @return true if the predator is a child or parent: False if the target is not a parent or child
     */

    private boolean IsTargetParentorChild(int targetID)
    {
        for (int id :ChildrenIDs)
        {
            if(id==targetID)
            {
                return true;
            }
        }
        if(targetID==p_parent_id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    /**
     * This function will evaluate the state of the animal and choose the best course of action for its next behavior
     * by reading the behavior flag and switching based on it.
     *
     * moving will be the flag until it reaches its destination, at which poin it will
     * @author d'Andre Clifton
     * @author Bradley Washburn
     */
    @Override
    void behavior() {
        //energy_level=10000;
        if(energy_level <= 0)
        {
            Dead = true;
        }
        if(inGestation)
        {
            currentGestationTime++;
            if(currentGestationTime>=GEST_PERIOD)
            {
                behavior="giveBirth";
            }
        }
        switch(behavior){
            case "moving":
                Move(X_dst,Y_dst);
                break;
            case "roam":
                hunt();
                Move(X_dst,Y_dst);
                break;
            case "mating":
                Move(mate.getX_pos(),mate.getY_pos());
                //Reproduce();
                break;
            case "giveBirth":
                GiveBirth();
                break;
            case "hunting":
                X_dst=target.getX_pos();
                Y_dst=target.getY_pos();
                hunt();
                Move(X_dst,Y_dst);
                break;
            case "CC":
                if(target instanceof  Predator) {
                    gameMaster.computeCombat(this, (Predator) target);
                    behavior = "";
                    return;
                }

                if(target instanceof  Grazer)
                {
                    gameMaster.computeCombat(this, (Grazer) target);
                    behavior = "";
                    return;
                }

            default:
                //calculate next behavior
                if(this.energy_level > ENERGY_TO_REPRODUCE)
                {
                    if(!inGestation)
                    {
                        if(matingList.size() > 0) {
                            double minX = 9000, minY = 9000;
                            int indexOfMin = 0;
                            for (int i = 0; i < matingList.size(); i++) {
                                if ((matingList.get(i).Y_pos - Y_pos) + (matingList.get(i).X_pos - this.X_pos) < (minX + minY)) {
                                    minX = matingList.get(i).getX_pos() - this.X_pos;
                                    minY = matingList.get(i).getY_pos() - this.Y_pos;
                                    indexOfMin = i;
                                }
                            }
                            mate=matingList.get(indexOfMin);
                            mate.mate=this;

                            //behavior = "mating";
                            X_dst = matingList.get(indexOfMin).X_dst;
                            Y_dst = matingList.get(indexOfMin).Y_dst;
                            behavior="mating";
                            Move(X_dst, Y_dst);
                            //Reproduce();
                        } else {
                            Roam();
                            behavior="roam";

                        }
                    }

                }
                else if(this.energy_level < ENERGY_TO_REPRODUCE)
                {
                    behavior = "hunting";//todo this flag needs to be unset at the end of hunt()
                    hunt();
                }
                break;
        }

        //if food is above threshold & can mate, mate
        //if food is above threshold and cannot mate, wander.
    }

    /**
     * Allows the pred to give birth
     * @author Bradley Washburn
     */
    private void GiveBirth()
    {
        DataBase db = DataBase.getInstance();
        ArrayList<Predator> toberemoved = new ArrayList<Predator>();
        for (Predator pred:db.getV_unbornPredators())
        {
            for (int childID : ChildrenIDs)
            {
                if(childID == pred.UID)
                {
                    toberemoved.addAll(db.PredatortoAdult(childID));
                }
            }
            behavior="default";

        }
        //Contains the preds that were born
        //and need to be removed from unborn
        for (Predator temp:toberemoved) {
            db.unbornPredators.remove(temp);

        }
        inGestation=false;
    }

    public char[] getP_aggression() {
        return p_aggression;
    }

    public double getP_speed() {
        return MAX_SPEED;
    }

    public char[] getP_power() {
        return p_power;
    }

    /**
     * this method will determine the closes animal in the visible_food array, then move towards that
     *
     * NOTE: We may want to consider making this function take an arg that is the
     * animal to be hunted, and remove the responsibility of determining the target to
     * be hunted from this method and moving to the switch  statement,
     * just before hunt() is called.
     *
     * @author d'andre clifton
     * @author Bradley Washburn
     */

    public void hunt() {
        //if animal food is low search for food
        double minX = 10000; //min X distance to move
        double minY = 10000; //Min Y distance to move
        int indexOfMin = -1;

        for (int i = 0; i < visible_food.size(); i++) // get i item in food array
        {

            char[] aggroGene = new char[]{'A', 'A'}; //used to compare this to AgrroGene in the following line

            if ((visible_food.get(i) instanceof Predator) && Arrays.equals(aggroGene, p_aggression)) //if it is a predator and this predator is aggressive
            {
                Predator target = (Predator) visible_food.get(i); //this line is used to get the target type
                if ((target.Y_pos - this.Y_pos) + (target.X_pos - this.X_pos) < (minX + minY)) {
                    minX = target.getX_pos() - this.X_pos;
                    minY = target.getY_pos() - this.Y_pos;
                    indexOfMin = i;
                }
            }

            if (visible_food.get(i) instanceof Grazer) {
                Grazer target2 = (Grazer) visible_food.get(i); //this line is used to get the target type
                if ((target2.Y_pos - this.Y_pos) + (target2.X_pos - this.X_pos) < (minX + minY)) {
                    minX = target2.getX_pos() - this.X_pos;
                    minY = target2.getY_pos() - this.Y_pos;
                    indexOfMin = i;
                }
            }
        }

        if(indexOfMin != -1) {
            X_dst = visible_food.get(indexOfMin).X_pos;
            Y_dst = visible_food.get(indexOfMin).Y_pos;
            Move(visible_food.get(indexOfMin).X_pos, visible_food.get(indexOfMin).Y_pos);
            this.target = visible_food.get(indexOfMin);
            if(target instanceof Grazer)
            {
                target.behavior = "fleeing";
                target.Roam();
                target.Move(target.X_dst,target.Y_dst);
            }
        } else
        {
            if(!(behavior.equals("roam")))
            {
                Roam();
                behavior="roam";
            }

        }
    }


    /**
     * Sets the pa-rent ID for the child
     * @param pID Parent ID
     */
    public void setParentID(int pID)
    {
        ParentID = pID;
    }

    /**
     * adds the children IDs to the list of childre
     * @param children child ID value
     */
    public void setChildrenIDs(Integer children)
    {
        ChildrenIDs.add(children);
    }



    public Animal getTarget() {
        return target;
    }
}

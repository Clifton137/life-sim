import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

//Source of Code + tutorial: https://docs.oracle.com/javase/8/javafx/get-started-tutorial/hello_world.htm
//Java sdk 8 (Appears in project structure as 1.8)

//Download for correct version of java SDK (1.8)
//https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

//Download for SceneBuilder 2.0 tool
//https://www.oracle.com/java/technologies/javafxscenebuilder-1x-archive-downloads.html

/**
 * GUI class that displays simulation (and simulation controls) to the user.
 * @author Bradley Bowen
 */
public class GUI extends Application {

    /**
     * @see javafx.application.Application#start
     */
    @Override
    public void start(Stage primaryStage) throws IOException { //Exception was required.

        //Loads fxml data before GUI window startup
        AnchorPane root = FXMLLoader.load(getClass().getResource("GUI.fxml"));

        //Prepares window, and set's window to specified dimensions at startup.
        Scene scene = new Scene(root, root.getPrefWidth(), root.getPrefHeight());

        primaryStage.setTitle("Team C Simulator");
        primaryStage.setScene(scene);


        //Makes GUI window visible to user.
        primaryStage.show();
    }

    @Override
    public void stop() {
       // System.out.println("GUI SHUTTING DOWN");
        Clock.shutdown();
    }

    /**
     * Main Function
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}












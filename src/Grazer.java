import java.util.ArrayList;

/**
 * Sim object that represents an animal that eats plants.
 */
public class Grazer extends Animal {
    private int ENERGY_INPUT;
    int current_energy;
    public Plant plantTarget;
    public Predator fleeTarget;
    ArrayList<Plant> foodArray= new ArrayList<>();
    public Plant target;
    public ArrayList<Plant> visible_food = new ArrayList<Plant>(7);
    private int hasBeenEatingCounter;
    boolean newBorn=false;

    /**
     * Constructor
     * @param xpos  contains x position
     * @param ypos  contains y position
     * @param startEnergy   contains starting energy level
     * @param reproduce contains energy to reproduce
     * @param maintainspeedtime time they can maintain speed
     * @param maxSpeed  max run speed
     * @param energyinputrate   energy input from eating something
     * @param energyoutputrate  energy used to move
     * @param energytoreproduce energy needed to reproduce
     */
    public Grazer(double xpos, double ypos, double startEnergy, int reproduce,
                  double maintainspeedtime, double maxSpeed,
                  int energyinputrate, int energyoutputrate, int energytoreproduce) {
        X_pos = xpos;
        Y_pos = ypos;
        energy_level = startEnergy;
        ENERGY_TO_REPRODUCE = reproduce;
        MAINTAIN_SPEED_TIME = maintainspeedtime;
        MAX_SPEED = maxSpeed;
        ENERGY_INPUT = energyinputrate;
        movement_EU = energyoutputrate;
        ENERGY_TO_REPRODUCE = energytoreproduce;
        hasBeenEatingCounter = 0;
    }


    public void Flee(Object TargetAnimal) {
        System.out.println("grazer Flee");
    }

    /**
     * Call this to check if the grazer can reproduce
     * Needs to be checked every second
     */
    public void Reproduce() {
        if (energy_level >= ENERGY_TO_REPRODUCE) {
            AnimalFactory factory = AnimalFactory.getInstance();
            factory.GrazerReproduce(energy_level, X_pos, Y_pos);
            energy_level=energy_level/2;
            behavior = "default";
        }

        behavior = "default";
    }


    @Override
    protected Object getTarget() {
        return target;
    }

    /**
     * Used to flag what behavior it should use this turn
     * @author Bradley Washburn
     */
    @Override
    void behavior() {
        //System.out.println(this.visible_food.);
        if (this.energy_level >= ENERGY_TO_REPRODUCE) {
            behavior = "reproduce";
        }
        if (energy_level <= 0) {
            this.Dead = true;
        }
        switch (behavior) {
            case "eating":
                Eat();
                break;
            case "roam":
                hunt();
                if (visible_food.size() == 0) {
                    behavior = "roam";
                }
                Move(X_dst,Y_dst);
                break;
            case "reproduce":
                Reproduce();
                break;
            case "fleeing":
                Move(X_dst, Y_dst);
                break;
            case "hunt":
                hunt();
                break;
            default:
                if (this.energy_level < ENERGY_TO_REPRODUCE) {
                    behavior = "hunt";
                    if(newBorn)
                    {
                        behavior="default";
                        DataBase db = DataBase.getInstance();
                        System.out.println(db.grazerList.size());
                        newBorn=false;
                        MoveNewBorns();
                    }
                    if (visible_food.size() == 0) {
                        behavior = "roam";
                        Roam();
                    } else {
                        this.hunt();
                    }

                }


        }

    }

    /**
     * Used to eat the targeted plant
     * @author Bradley Washburn
     */
    private void Eat() {
        //HasBeenFullSpeed=0;
        hasBeenEatingCounter++;
        if (hasBeenEatingCounter % 60 == 0) {
            energy_level = energy_level + ENERGY_INPUT;
        }
        DataBase db = DataBase.getInstance();

        for(Plant plant: db.plantList)
        {
            if (Math.abs(plant.getY_pos() - this.getY_pos()) <5 && Math.abs(plant.getY_pos() - this.getY_pos())<5 )
            {
                foodArray.add(plant);
            }
        }
        if(!foodArray.contains(target))
        {
            foodArray.add(target);
        }
        if (hasBeenEatingCounter >= 600)   //over ten minutes
        {
            //Kill all plants within the Grazers Radius
            for(Plant plant:foodArray)
            {
                db.plantList.remove(plant);
            }
            behavior = "default";
            foodArray.clear();
            visible_food.clear();
            target = null;
            hasBeenEatingCounter=0;
        }
    }

    /**
     * Used to track the plant the grazer is targeting and move to it
     * @author Bradley Washburn
     * @author D'Andre Cifton
     */
    private void hunt() {
        if (target != null) {
            if ((Math.abs(target.getX_pos() - this.getX_pos()) < 25) && Math.abs(target.getY_pos() - this.getY_pos()) < 25) {
                behavior = "eating";
                return;
            }
            Move(X_dst, Y_dst);

        } else {
            double minX = 10000; //min X distance to move
            double minY = 10000; //Min Y distance to move
            int indexOfMin = -1;

            for (int i = 0; i < visible_food.size(); i++) // get i item in food array
            {
                Plant target2 = visible_food.get(i); //this line is used to get the target type
                if ((target2.getY_pos() - this.getY_pos()) + (target2.getY_pos() - this.getY_pos()) < (minX + minY)) {
                    minX = target2.getX_pos() - this.X_pos;
                    minY = target2.getY_pos() - this.Y_pos;
                    indexOfMin = i;
                }
            }

            if (indexOfMin >= 0) {
                X_dst = visible_food.get(indexOfMin).getX_pos();
                Y_dst = visible_food.get(indexOfMin).getY_pos();
                Move(visible_food.get(indexOfMin).getX_pos(), visible_food.get(indexOfMin).getY_pos());
                this.target = visible_food.get(indexOfMin);
                behavior = "default";
            } else {
               Roam();
            }


        }

    }


    public void setAsNewBorn()
    {
        newBorn=true;
    }

    /**
     * Used to force the new borns to translate a few positions over so that they do not spawn
     * inside of their parents and stay there
     * @author Bradley Washburn
     */
    private void MoveNewBorns()
    {
        X_dst=X_pos+50;
        Y_dst=Y_pos+50;
        LifeSimDataParser parser = LifeSimDataParser.getInstance();
        if (X_dst > parser.getWorldWidth()) {
            X_dst = parser.getWorldWidth() - 5;
        }
        if (Y_dst > parser.getWorldHeight()) {
            Y_dst = parser.getWorldHeight() - 5;
        }
        if (X_dst <= 0) {
            X_dst = 5;
        }
        if (Y_dst <= 0) {
            Y_dst = 5;
        }
        Move(X_dst,Y_dst);
        GameMaster gm = GameMaster.getInstance();
        gm.UpdateFood(this);
    }
}

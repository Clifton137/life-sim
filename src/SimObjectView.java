import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @see javafx.scene.image.ImageView
 * @author Bradley Bowen
 */
public class SimObjectView extends ImageView {

    /** ID used to keep track of which object this image represents.
     * It should use the same int ID's used in other parts of the program.
     */
    private int objectID;


    /**
     * Constructor
     * @param image
     * @param objectID
     */
    SimObjectView(Image image, int objectID) {
        super(image);
        this.objectID = objectID;
    }

    /**
     * Getter
     * @return id
     */
    public int getObjectID() {
        return this.objectID;
    }

}

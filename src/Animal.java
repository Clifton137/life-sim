import java.util.ArrayList;
import java.util.Random;

/**
 * Class that Grazer and Predator inherit from.
 */
abstract class Animal
{
    protected double energy_level;
    protected double movement_EU;      //energy used to move
    public int vision_DU = 150; //changed to public, dont think it needs to be protected
    protected Boolean Dead = false;
    public double X_pos;
    public double Y_pos;
    protected double X_dst;
    protected double Y_dst;
    protected String behavior = "";

    public ArrayList<Object> visible_food;
    protected double MAX_SPEED;
    protected double MAINTAIN_SPEED_TIME;
    protected int reproductive_cooldown;
    public int ENERGY_TO_REPRODUCE;

    protected LocationSquare destination ;
    protected AnimalFactory AnimalFactory;
    protected int UID;      //The ID unique to each animal;
    protected GameMaster gameMaster;
    public ArrayList<Animal> targetsToIgnore = new ArrayList<Animal>();
    public int HasBeenFullSpeed=0;




    abstract void Reproduce();

    /**
     * @author D'Amdre Clifton
     * @param dstX
     * @param dstY
     * this function currently only allows for animals to run at full speed, also does not consider obstructions
     */

    public void Move(double dstX, double dstY) {
        energy_level=energy_level- movement_EU;
        double varaibleSpeed=MAX_SPEED;
        this.HasBeenFullSpeed++;
        if(this instanceof Grazer && HasBeenFullSpeed>=(MAINTAIN_SPEED_TIME)*60 && !(energy_level<=25))
        {
            varaibleSpeed=varaibleSpeed*0.75;
        }
        if(energy_level<=25 && this instanceof Grazer)
        {
            varaibleSpeed=10;
        }
        if (X_pos != dstX && Y_pos != dstY) { //if animal not in noether the correct row or column

            if (X_pos < dstX) {
                X_pos += varaibleSpeed / 2;
            } else {
                X_pos -= varaibleSpeed / 2;
            }

            if (Y_pos < dstY) {
                Y_pos += varaibleSpeed / 2;
            } else {
                Y_pos -= varaibleSpeed / 2;
            }

        } else if(this.X_pos != dstX) //animal is in correct row OR column, determine which one then move in a straight line from there
        {
            if (X_pos < dstX) {
                X_pos += varaibleSpeed;
            } else {
                X_pos -= varaibleSpeed;
            }
        }  else if (Y_pos != dstY) {
            if (Y_pos < dstY) {
                Y_pos += varaibleSpeed / 2;
            } else {
                Y_pos -= varaibleSpeed / 2;
            }
        }

        //Analyze what should be done after an animal reaches its target
        if( Math.abs(getX_pos() - X_dst)  < 25 && Math.abs(getY_pos() - Y_dst) < 25 ) // if animal is within 1 Du of target, consider it reached
        {
            if(behavior.equals("fleeing"))
            {
                behavior="default";
            }
            if(behavior.equals("mating"))
            {
                Reproduce();
            }
            if(behavior.equals("hunting"))
            {
                behavior = "CC";
                return;
            }
            if(behavior.equals("roam"))
            {
                //Roam();
                behavior="";
                return;
            }
            if(behavior.equals("eating"))
            {
                behavior="default";
            }
                behavior = "default";//this will cause the animal to re-evaluate its next behavior on the next tick
        }
    }

    protected abstract Object getTarget();

    /**
     * This method chooses a random LocationSquare within some range for the animal to move to, and set's it as the destination.
     */
    void Roam(){
        Random random = new Random();
        int x = random.nextInt(750);
        int y = random.nextInt(750);
        X_dst = x;
        Y_dst = y;
        if(!behavior.equals("fleeing"))
        {
            this.behavior = "roam";
        }
    };

    /**
     * This method sets this object's Dead variable to True
     */
    void Die(){
        this.Dead = true;
    };

    abstract void behavior();

    public double getX_pos() {
        return X_pos;
    }

    public double getY_pos() {
        return Y_pos;
    }

    public void setID(int ID){
        this.UID = ID;
    }

    int xs, ys; // Start coordinates

// Check point (xs, ys)




}
